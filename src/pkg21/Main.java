package pkg21;

import java.util.*;

public class Main 
{
   
    
    public static void main(String[] args) 
    {
        int Spelarhand = 0;
        int randomNum;
        int Dealerhand = 0;
        boolean Spelarenstur = true;
        Scanner scan = new Scanner(System.in);
        
        //första korten dras
        randomNum = 1 + (int)(Math.random()*10);
        Spelarhand = randomNum;
        System.out.println("Spelaren fick en " + randomNum);
        randomNum = 1 + (int)(Math.random()*10);
        Dealerhand = randomNum;
        System.out.println("Dealern fick en " + randomNum);
        randomNum = 1 + (int)(Math.random()*10);
        Spelarhand = Spelarhand + randomNum;
        System.out.println("Spelaren fick en " + randomNum + ", Spelaren har nu " + Spelarhand);
        
        while (true && Spelarhand < 21)
        {
            System.out.println("Du har " + Spelarhand +". Vill du dra ett kort y/n");
            String key = scan.nextLine();
            
            if(key.toLowerCase().equals("y"))
            {
                randomNum = 1 + (int)(Math.random()*10);
                Spelarhand = Spelarhand + randomNum;
                System.out.println("Spelaren fick en " + randomNum + ", Spelaren har nu " + Spelarhand);
            }
            
            else
            {
                break;
            }
        }
        
        if (Spelarhand > 21)
        {
            System.out.println("Spelaren förlorade");
            System.exit(1);
        }
        
        while (Dealerhand < 17)
        {
            randomNum = 1 + (int)(Math.random()*10); 
            
            Dealerhand = Dealerhand + randomNum;
            System.out.println("Dealern fick en " + randomNum + ", dealern har nu " + Dealerhand);
        }
     
        
        if (Dealerhand > 21 && Spelarhand <= 21)
        {
            System.out.println("Spelaren vann");
        }
        
        else if (Spelarhand > Dealerhand)
        {
            System.out.println("Spelaren vann");
        }
        
        else
        {
            System.out.println("Dealern vann");
        }
    }
    
}
